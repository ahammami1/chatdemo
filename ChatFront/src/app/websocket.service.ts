import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import * as Stomp from '@stomp/stompjs';
import * as SockJs from 'sockjs-client';


@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

 

  public connect() {
    let socket = new SockJs(`http://localhost:8080/socket`);

    let stompClient = Stomp.Stomp.over(socket);

    return stompClient;
}

  sendMessage(message:any){

    return this.http.post("http://localhost:8080/msg/send",message);

  }

  

  constructor(private http:HttpClient) { }
}
