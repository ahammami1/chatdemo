import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../websocket.service';
import { CompatClient } from '@stomp/stompjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  private msg : any[] = [];

  private message:any;

  private msgToSent ;

  private receiver;
  private sender;
  private stompClient :CompatClient;

  constructor(private wsService : WebsocketService) {

    this.connect();
 
    
   }


   connect(){

       // Open connection with server socket
       this.stompClient = this.wsService.connect();
       this.stompClient.connect({}, frame => {
   
     // Subscribe to notification topic
           this.stompClient.subscribe('/topic/notification', notifications => {
   
       // Update notifications attribute with the recent messsage sent from the serve
           // console.log(notifications)
             console.log(notifications);
            this.msg.push( JSON.parse(notifications.body).message);
           })
   
           
       });
   }

   pushMessageInTheTopic(){

    this.message = {
      receiver : {
        id : 1
      },
      sender : {
        id:2
      },
      message: this.msgToSent

    }

    this.stompClient.send('/app/send',{},JSON.stringify(this.message));

    this.stompClient.send('/topic/notification',{},JSON.stringify(this.message));

   }

   sendMessage(){

    this.message = {
      receiver : {
        id : this.receiver
      },
      sender : {
        id:this.sender
      },
      message: this.msgToSent

    }

    this.wsService.sendMessage(this.message).subscribe(data=> console.log(data))



   }

  ngOnInit() {
  }

}
