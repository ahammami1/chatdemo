package com.talan.recrutement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.talan.recrutement.persistence.entity.MessageEntity;
import com.talan.recrutement.persistence.repository.MessageRepository;

@Controller
public class NotifController {
	 @Autowired
	 private MessageRepository msgRepo;
	
	  @MessageMapping("/send") 
	  	@SendTo("/topic/notification")
		    public MessageEntity handle(MessageEntity msg) throws InterruptedException {
		  System.out.println("deded "+msg.getMessage());
		  msgRepo.save(msg);
		  
		  List<MessageEntity> msgs = msgRepo.findBySenderIdAndReceiverId(msg.getSender().getId(), msg.getReceiver().getId());
		  
		  msg = msgs.get(msgs.size()-1);
		  
		  Thread.sleep(10000);
		  
		  msg.setMessage("message from server :"+msg.getMessage());
		        return  msg;
		    }
	  



}
