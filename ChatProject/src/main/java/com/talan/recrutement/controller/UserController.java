package com.talan.recrutement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.talan.recrutement.persistence.entity.UserEntity;
import com.talan.recrutement.persistence.repository.UserRepository;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public UserEntity addUser(@RequestBody UserEntity newUser) {
		
		return userRepository.save(newUser);
		
		
	}
	
	
	

}
