package com.talan.recrutement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.talan.recrutement.persistence.entity.MessageEntity;
import com.talan.recrutement.persistence.repository.MessageRepository;



@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MessageController {
	
	 @Autowired
	 private SimpMessagingTemplate template;
	 
	 @Autowired
	 private MessageRepository msgRepo;
	 
	 @PostMapping(path = "/msg/send", produces = MediaType.APPLICATION_JSON_VALUE)

		public @ResponseBody MessageEntity boStatusUpdate(@RequestBody MessageEntity newMessage) {
		 MessageEntity msgEntity = msgRepo.save(newMessage);
		 template.convertAndSend("/topic/notification", msgEntity);

			return msgEntity;

		}

}
