package com.talan.recrutement.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talan.recrutement.persistence.entity.MessageEntity;


@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, Integer>{
	
	
	
	List<MessageEntity> findBySenderIdAndReceiverId(int idSender, int idReceiver);

}
