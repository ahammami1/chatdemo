package com.talan.recrutement.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "message")
public class MessageEntity implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	@Id
	private int id;
	
	
	

	@Column(name = "msg")
	private String message;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sender_id")
	private UserEntity sender;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "receiver_id")
	private UserEntity receiver;
	
	
	
	@Override
	public String toString() {
		return "MessageEntity [id=" + id + ", message=" + message + ", sender=" + sender + ", receiver=" + receiver
				+ "]";
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	public UserEntity getSender() {
		return sender;
	}



	public void setSender(UserEntity sender) {
		this.sender = sender;
	}



	public UserEntity getReceiver() {
		return receiver;
	}



	public void setReceiver(UserEntity receiver) {
		this.receiver = receiver;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((receiver == null) ? 0 : receiver.hashCode());
		result = prime * result + ((sender == null) ? 0 : sender.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageEntity other = (MessageEntity) obj;
		if (id != other.id)
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (receiver == null) {
			if (other.receiver != null)
				return false;
		} else if (!receiver.equals(other.receiver))
			return false;
		if (sender == null) {
			if (other.sender != null)
				return false;
		} else if (!sender.equals(other.sender))
			return false;
		return true;
	}
	
	
	
}	
