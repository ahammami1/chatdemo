package com.talan.recrutement.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talan.recrutement.persistence.entity.UserEntity;



@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer>{
	
	
	public UserEntity findByFirstName(String username);

}
